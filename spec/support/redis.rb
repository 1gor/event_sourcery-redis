# frozen_string_literal: true

module RedisHelper
  def new_redis_connection
    #Redis.connect(host: 'redis', port: ENV['BOXEN_REDIS_PORT'] || 6379)
    Redis.new(host: 'redis', port: ENV['BOXEN_REDIS_PORT'] || 6379)
  end
end

RSpec.configure do |config|
  config.include(RedisHelper)
end
