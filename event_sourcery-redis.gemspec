# frozen_string_literal: true

# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'event_sourcery/redis/version'

Gem::Specification.new do |spec|
  spec.name          = "event_sourcery-redis"
  spec.version       = EventSourcery::Redis::VERSION
  spec.authors       = ["Steve Hodgkiss"]
  spec.email         = ["steve@hodgkiss.me"]

  spec.summary       = %q{Implementation of a Redis EventStore for use with EventSourcery}
  spec.homepage      = "https://github.com/envato/event_sourcery-redis"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  # spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.files         = [".env",
                        ".gitignore",
                        ".rspec",
                        ".ruby-version",
                        ".travis.yml",
                        "Dockerfile",
                        "Gemfile",
                        "README.md",
                        "Rakefile",
                        "bin/console",
                        "bin/setup",
                        "docker-compose.yml",
                        "event_sourcery-redis.gemspec",
                        "lib/event_sourcery/redis.rb",
                        "lib/event_sourcery/redis/event_store.rb",
                        "lib/event_sourcery/redis/pub_sub_poll_waiter.rb",
                        "lib/event_sourcery/redis/tracker.rb",
                        "lib/event_sourcery/redis/version.rb",
                        "script/bench_writing_events.rb"]
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency             "event_sourcery"
  spec.add_dependency             "redis", "~>3.3.5"
  spec.add_dependency             "redis-lua"
  spec.add_dependency             "msgpack", "~> 1.1"
  spec.add_development_dependency "bundler", "~> 2.4"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "benchmark-ips"
  spec.add_development_dependency "hiredis"
end
