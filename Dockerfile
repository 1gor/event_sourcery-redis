########################
# Stage: ruby
FROM ruby:3.2-slim as ruby
LABEL description="Base ruby image used by other stages"

ARG APPNAME
ARG USERNAME=$APPNAME

# [Optional] Set the default user. Omit if you want to keep the default as root.
WORKDIR /home/$USERNAME

########################
# Stage: bundler
FROM ruby as developer
LABEL description="Install and cache gems for all environments"

RUN apt-get update && apt-get install -y \
    git \
    build-essential \
    && apt-get -q -y clean \
    && mkdir /bundle \
    && git config --global --add safe.directory /home/$USERNAME \
    && git config --global --add safe.directory /home/$USERNAME/vendor/event_sourcery \
    && rm -rf /var/lib/apt/lists/*

COPY . .

RUN bundle config --local path /bundle \
    && bundle install --quiet --jobs 4 --retry 3 \
    && rm -rf /bundle/ruby/*/cache

# RUN gem install bundler:1.17.3 \
#     && bundle config --local path /bundle \
#     && bundle install --quiet --jobs 4 --retry 3 \
#     && rm -rf /bundle/ruby/*/cache

CMD ["tail -f /dev/null"]
