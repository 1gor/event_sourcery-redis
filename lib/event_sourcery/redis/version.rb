# frozen_string_literal: true

module EventSourcery
  module Redis
    VERSION = "0.1.1"
  end
end
